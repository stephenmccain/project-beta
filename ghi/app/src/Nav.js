import { NavLink } from 'react-router-dom';
import NewSalesForm from "./sales/AddASale"
import Dropdown from 'react-bootstrap/Dropdown'
import FunFun from './confetti';



function refresh() {
  setTimeout(() => {  window.location.reload(); }, 1000);
}

function Nav() {


  return (
    <>

<nav className="navbar navbar-expand-md navbar-light bg-light bg-gradient bg-opacity-25">
<div className="container-fluid">
  <NavLink className="navbar-brand" to="/">Big Steve's Auto Emporium</NavLink>
  <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDarkDropdown" aria-controls="navbarNavDarkDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  </div>
  <div className="collapse navbar-collapse w-50 order-3 dual-collapse2" id="navbarNavDarkDropdown">
    <ul className="navbar-nav ml-auto">
      <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" to="/SalesHome" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          Sales
        </a>
        <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
          <li><a className="dropdown-item" href="/SalesHome">Sales Home </a></li>
          <li><a className="dropdown-item" href="/AddASale">Add Sale</a></li>
          <li><a className="dropdown-item" href="/AddSalesNewRep">Add Sale Rep</a></li>
        </ul>
      </li>
      <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          Service
        </a>
        <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
          <li><a className="dropdown-item" href="/service">Service</a></li>
          <li><a className="dropdown-item" href="/service/AddServiceForm">Schedule service</a></li>
          <li><a className="dropdown-item" href="/service/AddTechForm">Add Technician</a></li>
          
        </ul>
      </li>
      <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          Inventory
        </a>
        <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
          <li><a className="dropdown-item" href="/ListAutoInventory">Inventory</a></li>
          <li><a className="dropdown-item" href="/AddInventory" >Add To Inventory</a></li>
          <li><a className="dropdown-divider">________________</a></li>
          <li><a className="dropdown-item" href="/ListManufacturers">List Of Manufacturers</a></li>
          <li><a className="dropdown-item" href="/AddManufacturer" >Add New Manufacturer</a></li>
          <li><a className="dropdown-divider">________________</a></li>
          <li><a className="dropdown-item" href="/ListModels">List of Models</a></li>
          <li><a className="dropdown-item" href="/AddVehModel" >Add A Vehicle Model</a></li>

        </ul>
      </li>
      <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
          Customers
        </a>
        <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
          <li><a className="dropdown-item" href="/ListCustomers">Customer List</a></li>
          <li><a className="dropdown-item" href="/AddSalesCustomer">Add Customer</a></li>
        </ul>
      </li>
    </ul>
  </div>

</nav>
  </>  
  )
}

export default Nav;

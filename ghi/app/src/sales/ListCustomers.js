import React from "react";
import { Link, NavLink } from "react-router-dom"

class CustomerList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            customers: [],
            filter: "",
        }
        this.handleFilter = this.handleFilter.bind(this);
    }
    async componentDidMount() {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({customers: data.customers})
        }
    }
    handleFilter(event) {
        const value = event.target.value;
        this.setState({ filter:value })
    }
    render() {
        return (
            <>
            <div className="navbar navbar-expand-lg navbar-light bg-transparent">
            <div className="navbar-collapse collapse w-50 order-3 dual-collapse2">
                <ul className="navbar-nav m-auto">
                    <li className="nav-item"><NavLink to="/SalesHome" className="nav-link">Back To Sales Home</NavLink></li>
                </ul>
            </div>
            </div >
            <div className="input-group mb-3 ">
                <input onChange={this.handleFilter} type="text" className="form-control" placeholder="enter customer name to filter list" />
            </div>
            <table className="table table-striped table-hover">
		  <thead>
		    <tr>

		      <th scope="col">Name</th>
              <th scope="col">Address</th>
              <th scope="col">City</th>
              <th scope="col">State</th>
              <th scope="col">Phone</th>
              <th scope='col'>Email</th>
 
 		    </tr>
		  </thead>
		  <tbody>
            {this.state.customers.map(customers => {
              let toggle = ""
              if (customers.name  !== this.state.filter && this.state.filter !== "") {
                toggle = "d-none" }  
                return (
            <tr key={ customers.id}  className={toggle} >
            
		      <td>{ customers.name }</td>
              <td> { customers.address }</td>
              <td> { customers.city }</td>
              <td> {customers.state} </td>
              <td> {customers.phone }</td>
              <td> {customers.email} </td>
		      
		    </tr>
              );
            })}
		  </tbody>
		</table>   
            </>
        )
    }
}
 export default CustomerList
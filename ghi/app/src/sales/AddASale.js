import React from "react";
import { Link, NavLink } from "react-router-dom";
import FunFun from "../confetti";

import NewListOfSales from "./NewListOfSales";


 

class NewSalesForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sales: props.sales,
			employees: [],
            employee: "",
			sales_price: "",
			customer: "",
            customers: [],
			automobile: "",
            autos: [],
            filter: "",

        };
        this.handleEmployeeChange= this.handleEmployeeChange.bind(this);
        this.handleSalesPriceChange= this.handleSalesPriceChange.bind(this);
        this.handleCustomerChange= this.handleCustomerChange.bind(this);
        this.handleAutomobileChange= this.handleAutomobileChange.bind(this);
        this.handleIdChange= this.handleIdChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
    }

    handleEmployeeChange(event) {
        const value = event.target.value;
        this.setState( {employee: value})
    }

    handleSalesPriceChange(event) {
        const value = event.target.value;
        this.setState( {sales_price: value})
    }
    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState( {customer: value})
    }
    refresh() {
        setTimeout(() => {  window.location.reload(true); }, 3500);
        }
    handleAutomobileChange(event) {

        const value = event.target.value;
        for (const item of this.state.sales) {

            if (item.automobile.vin === value) {
                const span = document.getElementById("unable");
                const classes = span.classList;
                classes.add("d-none");

                const span1 = document.getElementById("warning");
                const classes1 = span1.classList;
                classes1.remove("d-none");

                this.refresh();

            
            }
        }
        this.setState( {automobile: value });
    }
    handleIdChange(event) {
        const value = event.target.value;
        this.setState( {id: value})
    }
    handleFilter(event) {
        const value = event.target.value;
        this.setState({ filter: value })
      }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};

        const submitDict = {
            employee: data.employee,
			sales_price: data.sales_price,
			customer: data.customer,
			automobile: data.automobile,
        }

        const locationUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(submitDict),
          headers: {
              'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
  
        if (response.ok) {  
          const newSale = await response.json();

          const span1 = document.getElementById("sale_successful");
          const classes1 = span1.classList;
          classes1.remove("d-none");
          this.refresh();

          const cleared = {
            employee: "",
			sales_price: "",
			customer: "",
			autos: [],

            };
            this.setState(cleared);

            this.getSales();
        } 
        else {
            const span = document.getElementById("warning");
            const classes = span.classList;
            classes.remove("d-none");
        }
    }
    async getEmployee(){
        const url = "http://localhost:8090/api/employees/";
        const response = await fetch(url);


        if (response.ok) {
            const data = await response.json();
            this.setState({employees: data.employees });
        } 
    }
    async getSales(){
        const url = "http://localhost:8090/api/sales/";

        const response = await fetch(url);


        if (response.ok) {
            const data = await response.json();
            this.setState({sales: data.sales });
        }
    }
    async getCustomer(){
        const url = "http://localhost:8090/api/customers/";

        const response = await fetch(url);


        if (response.ok) {
            const data = await response.json();
            this.setState({customers: data.customers });
        }
    }
    async getAutomobile(){
        const url = "http://localhost:8100/api/automobiles/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({autos: data.autos });
        }
    }
    async componentDidMount() {
        this.getEmployee()
        this.getSales()
        this.getCustomer()
        this.getAutomobile()
 
    }
    
    render() {
        
        return (
            <>

            <div className="navbar navbar-expand-lg navbar-light bg-transparent">

                <div className="navbar-collapse collapse w-50 order-3 dual-collapse2">
                    <ul className="navbar-nav m-auto">
                        <li className="nav-item"><NavLink to="/SalesHome"className="nav-link">Back To Sales Home</NavLink></li>
                        <li className="nav-item"><NavLink to="/AddSalesNewRep" className="nav-link" >Add New Sale Rep</NavLink></li>
                        <li className="nav-item"><NavLink to="/AddSalesCustomer" className="nav-link"  > Add Customer</NavLink></li>
                        <li className="nav-item"><NavLink to="/AddInventory" className="nav-link"  > Add To Inventory</NavLink></li>
                    </ul>
                </div>
     
        </div >
            <div className="row">
            <div className="offset-3 col-6">
                
              <div className="shadow p-4 mt-4">
                <h1>Add a New Sale </h1>
                <form onSubmit={this.handleSubmit} id="add-sale-form">
                <div className="mb-3">
              <select onChange={this.handleEmployeeChange} value={this.state.employee} required name="employee" id="employee" className="form-select"  > 
                    <option value="">Choose Sales rep</option>
                    {this.state.employees.map(employee => {
                        return (
                        <option key={employee.id} value={employee.id}>
                            {employee.name}
                        </option>
                        );
                    })}
                    </select>
              </div>
              <div className="mb-3">
              <select onChange={this.handleCustomerChange} value={this.state.customer} required name="customer" id="customer" className="form-select"  > 
                    <option value="">Choose Customer -If Not Listed Please Create One</option>
                    {this.state.customers.map(customer => {
                        return (
                        <option key={customer.id} value={customer.id}>
                            {customer.name}
                        </option>
                        );
                    })}
                    </select>
              </div>
              <div className="mb-3">
              <select onChange={this.handleAutomobileChange} value={this.state.automobile} required name="automobile" id="automobile" className="form-select"  > 
                    <option value="">Choose Vehicle By VIN</option>
                    {this.state.autos.map(auto => {
                        return (
                        <option key={auto.vin} value={auto.vin}>
                            {auto.vin}
                        </option>
                        );
                    })}
                    </select>
              </div>

                  <div className="form-floating mb-3">
                    <input onChange={this.handleSalesPriceChange} placeholder="sales_price" required type="number" name="sales_price" id="sales_price" className="form-control" value={this.state.sales_price}/>
                    <label htmlFor="sales_price">Sale Price</label>
                    
                  </div>

                    <div className="alert alert-danger alert-dismissible fade show  d-none" id="warning">
                    <strong>Error!</strong> THIS VEHICLE IS ALREADY SOLD! RELOADING PAGE IN 5 SECONDS.
                        <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                    </div>


                    <div className="alert alert-success alert-dismissible fade show  d-none" id="sale_successful">
                    <strong>GOOD WORK!!</strong> Nice Sale!
                    <small> *reload in 5 seconds*</small>
                        <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                    </div>
    
                  <button className="btn btn-secondary btn-sm" id="unable" >Create</button>
                </form>
              </div>
            </div>
          </div>
          <br></br>
          <h1 className="text-center"> Sold Vehicles </h1>
         
          <NewListOfSales/>


          </>
        );
      }
    }
    
    export default NewSalesForm;
    
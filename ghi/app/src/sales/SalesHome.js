import React from 'react';
import { Link, NavLink } from "react-router-dom";

 
import NewListOfSales from './NewListOfSales';


function SalesHome(props) {
  
    return (
          <>
        <h1 className="text-center"> Look Who is Selling Cars!</h1>
        <div className="navbar navbar-expand-lg navbar-light bg-transparent">

                <div className="navbar-collapse collapse w-50 order-3 dual-collapse2">
                    <ul className="navbar-nav m-auto">
                        <li className="nav-item"><NavLink to="/AddASale" className="nav-link">Add a Sale</NavLink></li>
                        <li className="nav-item"><NavLink to="/ListManufacturers" className="nav-link" >List Of Manufacturers</NavLink></li>
                        <li className="nav-item"><NavLink to="/ListModels" className="nav-link"  > List of Models</NavLink></li>
                        
                    </ul>
                </div>
         
        </div >



          <NewListOfSales/>

     

     </>  
    )
}

export default SalesHome
import React from "react";
import { Link, NavLink } from "react-router-dom"

class NewCustomerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
			name: "",
			address: "",
			city: "",
			state: "",
			phone: "",
      email: "",


        };
        this.handleNameChange= this.handleNameChange.bind(this);
        this.handleAddressChange= this.handleAddressChange.bind(this);
        this.handleCityChange= this.handleCityChange.bind(this);
        this.handleStateChange= this.handleStateChange.bind(this);
        this.handlePhoneChange= this.handlePhoneChange.bind(this);
        this.handleEmailChange= this.handleEmailChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState( {name: value})
    }

    handleAddressChange(event) {
        const value = event.target.value;
        this.setState( {address: value})
    }

    handleCityChange(event) {
        const value = event.target.value;
        this.setState( {city: value})
    }
    handleStateChange(event) {
        const value = event.target.value;
        this.setState( {state: value})
    }
    handlePhoneChange(event) {
        const value = event.target.value;
        this.setState( {phone: value})
    }
    handleEmailChange(event) {
      const value = event.target.value;
      this.setState(  {email: value})
    }
    refresh() {
      setTimeout(() => {  window.location.reload(true); }, 3500);
      }



    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};


        const locationUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
  
        if (response.ok) {  
          const newCustomer = await response.json();
      
          const span = document.getElementById("added_customer");
          const classes = span.classList;
          classes.remove("d-none");

          this.refresh();

          const cleared = {
            name: "",
            address: "",
            city: "",
            state: "",
            phone: "",
            email: "",

            };
            this.setState(cleared);
        } else {
            const span = document.getElementById("warning");
            const classes = span.classList;
            classes.remove("d-none");
        }
    }

    async componentDidMount() {
    
        const url = "http://localhost:8090/api/customers/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({customer: data.customer});
        } 
    }
    render() {
        return (
            <>
            <div className="navbar navbar-expand-lg navbar-light bg-transparent">
            <div className="navbar-collapse collapse w-50 order-3 dual-collapse2">
                <ul className="navbar-nav m-auto">
                    <li className="nav-item"><NavLink to="/SalesHome" className="nav-link">Back To Sales Home</NavLink></li>
                    <li className="nav-item"><NavLink to="/ListCustomers" className="nav-link">Back To Customer list</NavLink></li>
                </ul>
            </div>
            </div >
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a Customer </h1>
                <form onSubmit={this.handleSubmit} id="add-customer-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name}/>
                    <label htmlFor="name">Name
                    </label>
                    
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleAddressChange} placeholder="address" required type="text" name="address" id="address" className="form-control" value={this.state.address}/>
                    <label htmlFor="address">Street Address</label>
                    
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleCityChange} placeholder="city" required type="text" name="city" id="city" className="form-control" value={this.state.city}/>
                    <label htmlFor="city">City</label>
                    
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleStateChange} placeholder="state" required type="text" name="state" id="state" className="form-control" value={this.state.state}/>
                    <label htmlFor="state">State</label>
                    
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePhoneChange} placeholder="phone" required type="tel"  name="phone" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" id="phone" className="form-control" value={this.state.phone}/>
                    <label htmlFor="phone">Phone </label>
                    <small className="text-secondary">Format: 123-456-7890</small>
                    
                    
                  </div>

                  <div className="form-floating mb-3">
                    <input onChange={this.handleEmailChange} placeholder="email" required type="text"  name="email"  id="email" className="form-control" value={this.state.email}/>
                    <label htmlFor="email">Email </label>
                    <small className="text-secondary">Format: email@email.com</small>
                    
                    
                  </div>

                  <div className="alert alert-danger alert-dismissible fade show  d-none" id="warning">
                    <strong>Error!</strong> The Please Try Again.
                        <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                    </div>

                    <div className="alert alert-success alert-dismissible fade show  d-none" id="added_customer">
                    <strong>Customer Added</strong>  
                    <small> *reload in 5 seconds*</small>
                        <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                    </div>
    
                  <button className="btn btn-secondary btn-sm">Create</button>
                </form>
              </div>
            </div>
          </div>
          </>
        );
      }
    }
    
    export default NewCustomerForm;
import React from "react";
import '../index.css';

class NewListOfSales extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sales: [],
            filter: "",
        }
        this.handleFilter = this.handleFilter.bind(this);
    }
    async componentDidMount() {
        const url ="	http://localhost:8090/api/sales/";
        const response =await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ sales: data.sales})
        }
    }
    handleFilter(event) {
        const value = event.target.value;
        this.setState({ filter: value })
      }
    
    render() {
        return (
            <>
             <div className="input-group mb-3 ">
                <input onChange={this.handleFilter} type="text" className="form-control" placeholder="enter employee name to filter list" />
            </div>
            <table className="table table-striped">
		  <thead>
		    <tr>

		      <th scope="col">Sales Rep</th>
		      <th scope="col">Customer</th>
		      <th scope="col">VIN</th>
		      <th scope="col">Sales Price</th>
 
		    </tr>
		  </thead>
		  <tbody>
            {this.state.sales.map(sale => {
                let toggle = ""
                if (sale.employee.name  !== this.state.filter && this.state.filter !== "") {
                  toggle = "d-none" }  
                return (
            <tr key={ sale.automobile.vin }  className={toggle} >

		      <td>{ sale.employee.name }</td>
		      <td>{ sale.customer.name }</td>
		      <td>{ sale.automobile.vin }</td>
		      <td> ${ sale.sales_price }</td>
		    </tr>
              );
            })}
		  </tbody>
		</table>   
            </>

        )
       
    }
}

export default NewListOfSales

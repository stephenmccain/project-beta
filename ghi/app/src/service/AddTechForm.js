import React from 'react';
import ServiceNav from "./ServiceNav";

class AddTechForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            employee_number: "",
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeID = this.handleChangeID.bind(this);
    }

    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }
    handleChangeID(event) {
        const value = event.target.value;
        this.setState({ employee_number: value });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };

        const url = 'http://localhost:8080/api/technician/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newService = await response.json();
            this.setState({
                name: "",
                employee_number: "",
            });
            alert("Employee successfully added!")
        }
        else {
            alert("Employee number is already in use!");
            this.setState({
                employee_number: "",
            });
        }
    }

    render() {
        return (
            <>
                <ServiceNav />
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <form onSubmit={this.handleSubmit} id="create-tech-form">
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleChangeName} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                                    <label htmlFor="name">name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleChangeID} value={this.state.employee_number} placeholder="employee_number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                                    <label htmlFor="owner">employee number</label>
                                </div>
                                <button className="btn btn-primary">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default AddTechForm;

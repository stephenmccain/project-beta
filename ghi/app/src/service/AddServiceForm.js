import React from 'react';
import ServiceNav from "./ServiceNav";


class AddServiceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: "",
            owner_name: "",
            date_of_service: "",
            time_of_service: "",
            technician_id: "",
            reason_for_service: "",
            technicians: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeVIN = this.handleChangeVIN.bind(this);
        this.handleChangeOwner = this.handleChangeOwner.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleChangeTime = this.handleChangeTime.bind(this);
        this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
        this.handleChangeReason = this.handleChangeReason.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/technician/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ technicians: data.technicians });
        }
    }
    handleChangeVIN(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }
    handleChangeOwner(event) {
        const value = event.target.value;
        this.setState({ owner_name: value });
    }
    handleChangeDate(event) {
        const value = event.target.value;
        this.setState({ date_of_service: value });
    }
    handleChangeTime(event) {
        const value = event.target.value;
        this.setState({ time_of_service: value });
    }
    handleChangeTechnician(event) {
        const value = event.target.value;

        this.setState({ technician_id: value });
    }
    handleChangeReason(event) {
        const value = event.target.value;
        this.setState({ reason_for_service: value });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.technicians;

        const url = 'http://localhost:8080/api/service/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newService = await response.json();
            this.setState({
                vin: "",
                owner_name: "",
                date_of_service: "",
                time_of_service: "",
                technician_id: "",
                reason_for_service: "",
            });
        }
    }

    render() {
        return (
            <>
                <ServiceNav />
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <form onSubmit={this.handleSubmit} id="create-tech-form">
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleChangeVIN} value={this.state.vin} placeholder="VIN" required type="text" minLength="17"  maxLength="17" name="vin" id="vin" className="form-control" />
                                    <label htmlFor="vin">vin</label>
                                    <small className="text-secondary">a vin is 17 characters </small>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleChangeOwner} value={this.state.owner_name} placeholder="Style" required type="text" name="owner_name" id="owner_name" className="form-control" />
                                    <label htmlFor="owner">owner</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleChangeDate} value={this.state.date_of_service} placeholder="date_of_service" required type="text" name="date_of_service" id="date_of_service" className="form-control" />
                                    <label htmlFor="date">date of service</label>
                                    <small className="text-secondary">format: YYYY-MM-DD </small>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleChangeTime} value={this.state.time_of_service} placeholder="time_of_service" required type="text" name="time_of_service" id="time_of_service" className="form-control" />
                                    <label htmlFor="time">time of service</label>
                                    <small className="text-secondary">format: HH:MM:SS </small>
                                </div>
                                <div className="mb-3">
                                    <select onChange={this.handleChangeTechnician} value={this.state.technician_id} required name="technician" id="technician" className="form-select">
                                        <option value="">select technician</option>
                                        {this.state.technicians.map(technician_id => {
                                            return (
                                                <option key={technician_id.employee_number}
                                                    value={technician_id.employee_number}>{technician_id.name}</option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleChangeReason} value={this.state.reason_for_service} placeholder="reason_for_service" required type="text" name="reason_for_service" id="reason_for_service" className="form-control" />
                                    <label htmlFor="time">reason for service</label>
                                </div>
                                <button className="btn btn-outline-secondary">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default AddServiceForm;

import React from "react";
import ServiceNav from "./ServiceNav";
import '../index.css';

class ServiceList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active_requests: [],
      filter: "",
    };
    this.handleFilter = this.handleFilter.bind(this);
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/service/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ active_requests: data.active_requests })
    }
  }

  async handleDelete(id) {
    const url = `http://localhost:8080/api/service/${id}/`;
    const fetchConfig = {
      method: "DELETE"
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      window.location.reload();
    }

  }
  async handleComplete(id) {
    const updateURl = `http://localhost:8080/api/service/${id}/`;
    const fetchConfig = {
      method: "PUT",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ completed: true })
    };
    const response = await fetch(updateURl, fetchConfig);
    if (response.ok) {
      this.setState({ completed: true });
      window.location.reload();
    };
  }
  handleFilter(event) {
    const value = event.target.value;
    this.setState({ filter: value })
  }


  render() {
    return (
      <>
        <ServiceNav />
        <div className="input-group mb-3 ">
          <input onChange={this.handleFilter} type="text" className="form-control" placeholder="enter vin to filter list" />
        </div>
        <table className='table text-center align-middle table-borderless table-hover'>
          <thead>
            <tr>
              <th>VIN</th>
              <th>Owner</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
            </tr>
          </thead>
          <tbody>
            {this.state.active_requests.map((service) => {
              let completed_cancel = "btn btn-outline-danger btn-space"
              let completed_success = "btn btn-outline-success btn-space"
              let complete = "complete"
              let notice = ""
              if (service.vip) {
                notice = "table-info"
              }
              if (service.completed) {
                completed_cancel = "d-none"
                completed_success = "btn btn-success disabled"
                complete = "completed"
              }
              let toggle = ""
              if (service.vin !== this.state.filter && this.state.filter !== "") {
                toggle = "d-none"
              }

              return (
                <tr className={toggle} key={service.id}>
                  <td className={notice}>{service.vin}</td>
                  <td className={notice}>{service.owner_name}</td>
                  <td className={notice}>{service.date_of_service}</td>
                  <td className={notice}>{service.time_of_service}</td>
                  <td className={notice}>{service.technician.name}</td>
                  <td className={notice}>{service.reason_for_service}</td>
                  <td>
                    <button className={completed_success} onClick={() => this.handleComplete(service.id)} to="">{complete}</button>
                    <button className={completed_cancel} onClick={() => this.handleDelete(service.id)} to="">cancel</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <p>*VIP customers are noted above in blue</p>
      </>
    )
  }
}
export default ServiceList;

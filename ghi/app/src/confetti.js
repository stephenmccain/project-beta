import React, { useState, useRef, useEffect } from "react";
import Confetti from "react-confetti";
// import "./styles.css";
import Nav from "./Nav";

export default function FunFun(props) {
    const [height, setHeight] = useState(null);
    const [width, setWidth] = useState(null);
    const [show, setShow] = useState(false);
    const confettiRef = useRef(null);
  
    useEffect(() => {
      setHeight(confettiRef.current.clientHeight);
      setWidth(confettiRef.current.clientWidth);
    }, [])
  
    const handleShow = toggle => {
      setShow(toggle);
    }
  
    return (
        
      <div className="FunFun">
          {props.renderComponent}
        <div
          onMouseEnter={() => handleShow(true)}
        //   onMouseLeave={() => handleShow(false)}
          className="confetti-wrap"
          ref={confettiRef}>

          <Confetti
            recycle={show}
            numberOfPieces={500}
            width={width*2}
            height={height*5}
          />
        </div>
      </div>
    );
  }
  
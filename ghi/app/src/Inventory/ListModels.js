import React from 'react';
import { Link, NavLink } from "react-router-dom"


function ModelsList(props) {

    return (
        <>
        <nav>
          <h1 className="text-center"> All Vehicle Models</h1>
            <div className="navbar navbar-expand-lg navbar-light bg-transparent">
                <div className="navbar-collapse collapse w-50 order-3 dual-collapse2">
                  <ul className="navbar-nav m-auto">
                  <li className="nav-item"><NavLink to="/SalesHome" className="nav-link">Back To Sales Home</NavLink></li>
                      <li className="nav-item"><NavLink to="/AddVehModel" className="nav-link">Add A Vehicle Model</NavLink></li>
                  </ul>
                </div>
            </div >
        </nav>
        
      <div className="container">
      <div className="row">
        <div className="col-12">
  

          <table className="table table-striped table-hover">
            <thead>
              <tr>

                <th scope="col">Name</th>
                    <th scope="col">Manufacturer</th>
                    <th scope="col">Picture</th>
              </tr>
            </thead>
            <tbody>
                  {props.model.models.map(model => {
                      return (
                  <tr key={ model.id}   >
                  
                <td>{ model.name }</td>
                    <td> {model.manufacturer.name}</td>
                <td className="w-25">
                  <img src={model.picture_url}  className="img-fluid img-thumbnail" alt=""/>
                        </td>
              </tr>
                    );
                  })}
            </tbody>
          </table>   
        </div>
      </div>
    </div>

     </>  
    )
}

export default ModelsList
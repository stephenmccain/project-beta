import React from "react";
import { Link, NavLink } from "react-router-dom"
 

class AddInventory extends React.Component {
    constructor(props) {
     
        super(props);
        this.state = {
            color:"",
            year:"",
            vin:"",
            model_id:"",
            models:[],

        };
        
        this.handleColorChange= this.handleColorChange.bind(this);
        this.handleYearChange= this.handleYearChange.bind(this);
        this.handleVinChange= this.handleVinChange.bind(this);
        this.handleModelChange= this.handleModelChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
    }
    handleYearChange(event) {
        const value = event.target.value;
        this.setState( { year: value})
    }

    handleVinChange(event) {
        const value = event.target.value.toUpperCase();
        this.setState( {vin: value})
    }
    handleModelChange(event) {
        const value = event.target.value;
        this.setState( {model_id: value})
    }
    refresh() {
      setTimeout(() => {  window.location.reload(); }, 1000);
    }

    
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.models;
 


        const locationUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
      
  
        if (response.ok) {  
          const newInventory = await response.json();

      

          const span = document.getElementById("added_vehicle");
          const classes = span.classList;
          classes.remove("d-none");

          this.refresh();


          const cleared = {
            color: "",
            year: "",
            vin:"",
            model_id:"",

            };
            this.setState(cleared);
            
        } else {
            const span = document.getElementById("warning");
            const classes = span.classList;
            classes.remove("d-none");
            this.refresh();
        }
    }

    async componentDidMount() {
    
        const url = "http://localhost:8100/api/models/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({models: data.models});
        }
 
    }
    render() {
        return (
            <>
            <nav>
            <div className="navbar navbar-expand-lg navbar-light bg-transparent">
            <div className="navbar-collapse collapse w-50 order-3 dual-collapse2">
                <ul className="navbar-nav m-auto">
                    <li className="nav-item"><NavLink to="/SalesHome" className="nav-link">Back To Sales Home</NavLink></li>
                    <li className="nav-item"><NavLink to="/ListAutoInventory" className="nav-link">Back to Current Inventory</NavLink></li>
                    <li className="nav-item"><NavLink to="/ListManufacturers" className="nav-link">List Of Manufacturers</NavLink></li>
                    <li className="nav-item"><NavLink to="/ListModels" className="nav-link">List of Models</NavLink></li>
                </ul>
            </div>
            </div >
            </nav>
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add Vehicle To the Inventory </h1>
                <form onSubmit={this.handleSubmit} id="add-inventory-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" value={this.state.color}/>
                    <label htmlFor="color">Color</label>
                    
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleYearChange} placeholder="year" required type="number" min="1960" max="2030"  name="year" id="year" className="form-control" value={this.state.year}/>
                    <label htmlFor="year">Year</label>
                    <small className="text-secondary">1960-2030</small>

                  </div>
                  <div className="form-floating mb-3">
                    <input  onChange={this.handleVinChange}  placeholder="vin" required type="text" minLength="17"  maxLength="18"  name="vin" id="vin"  className="form-control"  value={this.state.vin}/>
                    <label htmlFor="vin">VIN</label>
                    <small className="text-secondary">Example: 1C3CC5FB587456321 </small>
                    
                  </div>

                  <div className="mb-3">
              <select onChange={this.handleModelChange} value={this.state.model_id} required name="model_id" id="model_id" className="form-select"  > 
                    <option value="">Choose Vehicle Model</option>
                    {this.state.models.map(models => {
                        return ( 
                        <option key={models.id} value={models.id}>
                            {models.name}
                        </option>
                        );
                    })}
                    </select>
                    <small className="text-secondary">If Model Is Not Listed, Please:   
                    <Link to="/AddVehModel" type="button" className="btn btn-link">Add A Vehicle Model</Link>
                    </small>
                  
              </div>
                  
                  <div className="alert alert-danger alert-dismissible fade show  d-none" id="warning">
                    <strong>Error!</strong> Try Again Please.
                        <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                    </div>

                    <div className="alert alert-success alert-dismissible fade show  d-none" id="added_vehicle">
                    <strong>Vehicle Added</strong>  
                    <small> *reloading page*</small>
                        <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                    </div>
    
                  <button className="btn btn-secondary btn-sm">Create</button>
                </form>
              </div>
            </div>
          </div>
          </>
        );
      }
    }
    
    export default AddInventory;
    
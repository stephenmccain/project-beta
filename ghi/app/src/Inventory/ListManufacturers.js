import React from 'react';
import { Link, NavLink } from "react-router-dom"


function ManufacturerList(props) {

  
    return (
          <>
         
        <h1 className="text-center"> List of Manufacturers { }</h1>
        <div className="navbar navbar-expand-lg navbar-light bg-transparent">
            <div className="navbar-collapse collapse w-50 order-3 dual-collapse2">
                <ul className="navbar-nav m-auto">
                <li className="nav-item"><NavLink to="/SalesHome" className="nav-link">Back To Sales Home</NavLink></li>
                    <li className="nav-item"><NavLink to="/AddManufacturer" className="nav-link">Add New Manufacturer</NavLink></li>
                </ul>
            </div>
        </div >
        
      <div className="container">
  <div className="row">
    <div className="col-12">
 

		<table className="table table-striped">
		  <thead>
		    <tr>

		      <th scope="col">Name</th>
 		    </tr>
		  </thead>
		  <tbody>
            {props.manufacturer.manufacturers.map(manufacturers => {
                return (
            <tr key={ manufacturers.name}   >

		      <td>{ manufacturers.name }</td>

		    </tr>
              );
            })}
		  </tbody>
		</table>   
    </div>
  </div>
</div>

     </>  
    )
}

export default ManufacturerList
import React from 'react';
import { Link, NavLink } from "react-router-dom"




function InventoryList(props) {

  
    return (
          <>
  
        <h1 className="text-center"> Current Vehicle Inventory {  }

        </h1>
        <div className="navbar navbar-expand-lg navbar-light bg-transparent">
            <div className="navbar-collapse collapse w-50 order-3 dual-collapse2">
                <ul className="navbar-nav m-auto">
                    <li className="nav-item"><NavLink to="/AddInventory" className="nav-link">Add To Inventory</NavLink></li>
                </ul>
            </div>
        </div >
      <div className="container">
  <div className="row">
    <div className="col-12">
 

		<table className="table table-striped table-hover">
		  <thead>
		    <tr>

              <th scope="col">VIN</th>
              <th scope="col">Color</th>
              <th scope="col">Year</th>
              <th scope="col">Model</th>
              <th scope="col">Manufacturer</th>
              <th scope='col'>Picture</th>
 		    </tr>
		  </thead>
		  <tbody>
      

              
              
            {props.inventory.autos.map (auto => {
             
                return (
            <tr key={ auto.id }   >
            <td> </td>
		      <td className="text-capitalize"> { auto.vin }   </td>
              <td> { auto.color }</td>
              <td> { auto.year }</td>
              <td> {auto.model.name} </td>
              <td> {auto.model.manufacturer.name }</td>
		      <td className="w-25">
			      <img src={auto.model.picture_url}  className="img-fluid img-thumbnail" alt=""/>
                  </td>
		    </tr>
        
              );
           })}
		  </tbody>
		</table> 
    <div className="alert alert-danger alert-dismissible fade show  d-none" id="warning">
                    <strong>Error!</strong> The Customer ID MUST be Unique, try again.
                        <button type="button" className="btn-close" data-bs-dismiss="alert"></button>
                    </div>  
    </div>
  </div>
</div>

     </>  
    )
}

export default InventoryList
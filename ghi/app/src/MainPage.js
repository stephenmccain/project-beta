import React from 'react';
import { Link } from "react-router-dom"



function MainPage(props) {
  return (
    <>
    <div className="px-4 py-5 my-5 text-center">

      <h1 className="display-5 fw-bold">
      <img src="https://upload.wikimedia.org/wikipedia/commons/c/c1/Blue%2C_red_and_white_ballon.svg" 
        className="border-0" alt="Responsive image"  width="150" height="150"/> { }Welcome to 
                <img src="https://upload.wikimedia.org/wikipedia/commons/c/c1/Blue%2C_red_and_white_ballon.svg" 
        className="border-0" alt="Responsive image"  width="150" height="150"/>
        <br></br>BIG STEVE'S AUTO EMPORIUM {  }
        </h1>
        <h1 className="p-3 mb-2 bg-secondary text-white">
        GOOD CREDIT? BAD CREDIT? NO CREDIT? 
            <br></br>
            We Can Sell You A Car Today! 
        </h1>
    </div>
    <div className="container">
  <div className="row">
    <div className="col-12">
 
    <h1 className="text-center"> Featured Vehicle Inventory </h1>
		<table className="table table-striped table-hover">
		  <thead>
		    <tr>

		      <th scope="col">VIN</th>
              <th scope="col">Color</th>
              <th scope="col">Year</th>
              <th scope="col">Model</th>
              <th scope="col">Manufacturer</th>
              <th scope='col'>Picture</th>
 		    </tr>
		  </thead>
		  <tbody>
            {props.inventory.autos.map(auto => {
                return (
            <tr key={ auto.id}   >
            
		      <td>{ auto.vin }</td>
              <td> { auto.color }</td>
              <td> { auto.year }</td>
              <td> {auto.model.name} </td>
              <td> {auto.model.manufacturer.name }</td>
		      <td className="w-25">
			      <img src={auto.model.picture_url}  className="img-fluid img-thumbnail" alt=""/>
                  </td>
		    </tr>
              );
            })}
		  </tbody>
		</table>  
    </div>
    </div>
    </div>
    </>
    
  );
}

export default MainPage;

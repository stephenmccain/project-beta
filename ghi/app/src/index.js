import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import "bootstrap/dist/css/bootstrap.min.css";

const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadData() {
  let ServiceData, SalesData, ManufacturerData, ModelData, InventoryData, CustomerData;
  const SalesResponse = await fetch('http://localhost:8090/api/sales/');
  const ServiceResponse = await fetch('http://localhost:8080/api/service/');
  const ManufacturerResponse = await fetch('http://localhost:8100/api/manufacturers/');
  const ModelResponse = await fetch('http://localhost:8100/api/models/');
  const InventoryResponse = await fetch("http://localhost:8100/api/automobiles/");
  const CustomerResponse = await fetch("http://localhost:8090/api/customers/");

  if (SalesResponse.ok) {
    SalesData = await SalesResponse.json();

  } else {
    console.error(SalesResponse);
  }

  if (ServiceResponse.ok) {
    ServiceData = await ServiceResponse.json();
  }
  else {
    console.error(ServiceResponse);
  }

  if (ManufacturerResponse.ok) {
    ManufacturerData = await ManufacturerResponse.json();
  } else {
    console.error(ManufacturerResponse);
  }

  if (ModelResponse.ok) {
    ModelData = await ModelResponse.json();
  } else {
    console.error(ModelResponse);
  }

  if (InventoryResponse.ok) {
    InventoryData = await InventoryResponse.json();
  } else {
    console.error(InventoryResponse);
  }
  if (CustomerResponse.ok) {
    CustomerData = await CustomerResponse.json();
  } else {
    console.error(CustomerResponse);
  }
  root.render(
    <React.StrictMode>
      <App service={ServiceData} sales={SalesData.sales} manufacturer={ManufacturerData} model={ModelData} inventory={InventoryData} customer={CustomerData}/>
    </React.StrictMode>
  );
}

loadData();

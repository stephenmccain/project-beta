import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import FunFun from './confetti';



import ListManufacturers from './Inventory/ListManufacturers';
import AddManufacturer from './Inventory/AddManufacturer';
import ListModels from './Inventory/ListModels';
import AddVehModel from'./Inventory/AddVehModel';
import ListAutoInventory from './Inventory/ListAutoInventory';
import AddInventory from './Inventory/AddInventory';

import AddASale from './sales/AddASale';

import ListCustomers from './sales/ListCustomers';
import SalesHome from './sales/SalesHome';
import AddSalesNewRep from './sales/AddSalesNewRep';
import AddSalesCustomer from './sales/AddSalesCustomer';

import ServiceList from './service/ListServiceRequests';
import AddServiceForm from './service/AddServiceForm';
import AddTechForm from './service/AddTechForm';



function App(props) {
  return (
    <BrowserRouter>

      <FunFun renderComponent={<Nav />} />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage inventory={props.inventory}/>} />
          <Route path="/SalesHome" element={<SalesHome sales={props.sales} />} />
          <Route path="/AddSalesNewRep" element={<AddSalesNewRep />} />
          <Route path="/AddSalesCustomer" element={<AddSalesCustomer />} />
          <Route path="/ListCustomers" element={<ListCustomers customer={props.customer}/>} />
          <Route path="/AddASale" element={<FunFun renderComponent={<AddASale sales={props.sales} />} />}/>

      
          <Route path="service">
            <Route index element={<ServiceList services={props.service} />} />
            <Route path="AddServiceForm" element={<AddServiceForm />} />
            <Route path="AddTechForm" element={<AddTechForm />} />
          </Route>
          <Route path="/ListManufacturers" element={<ListManufacturers manufacturer={props.manufacturer} />} />
          <Route path="/AddManufacturer" element={<AddManufacturer />} />
          <Route path="/ListModels" element={<ListModels model={props.model} />} />
          <Route path="/AddVehModel" element={<AddVehModel />} />
          <Route path="/ListAutoInventory" element={<ListAutoInventory inventory={props.inventory} />} />
          <Route path="/AddInventory" element={<AddInventory inventory={props.inventory}/>} />
          <Route path="/confetti" element={<FunFun/>} />
        </Routes>

      </div>
    </BrowserRouter>

    
  );
}

export default App;

from common.json import ModelEncoder
from .models import Service, Technician, AutomobileVO

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
    ]

class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "id",
        "vin",
        "owner_name",
        "vip",
        "date_of_service",
        "time_of_service",
        "reason_for_service",
        "technician",
        "completed",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }

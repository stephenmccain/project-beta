from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=20, unique=False, default=False)



class Technician(models.Model):
    name = models.CharField(max_length=200, unique=False)
    employee_number = models.PositiveIntegerField(primary_key=True)


class Service(models.Model):
    vin = models.CharField(max_length=17, unique=False)
    owner_name = models.CharField(max_length=200, unique=False)
    date_of_service = models.DateField(null=True)
    time_of_service = models.TimeField(null=True)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.PROTECT,
    )
    reason_for_service = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)
    completed = models.BooleanField(default=False)
